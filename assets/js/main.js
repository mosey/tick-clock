$(document).ready(function() {

	
	function tickTock() {
		var date = new Date();
		var hours = String(date.getHours());
		var minutes = String(date.getMinutes());
		var seconds = String(date.getSeconds());
		//var minutes = String(date.getMinutes());
		//var seconds = String(date.getSeconds());

		if(minutes < 10) {
			minutes = '0' + minutes;
		}
		if(seconds < 10) {
			seconds = '0' + seconds;
		}
		if (hours.length === 1) {
			hours = '0' + hours;
		} 
		var time = hours + minutes + seconds;
			
		function removeClasses() {
			$('#first, #second, #third, #fourth,#fifth,#sixth').removeAttr('class');
		}
		
		removeClasses();

		function addClasses() {
			$('#first').addClass('digit time-'+time[0]);
			$('#second').addClass('digit time-'+time[1]);
			$('#third').addClass('digit time-'+time[2]);
			$('#fourth').addClass('digit time-'+time[3]);
			$('#fifth').addClass('digit time-'+time[4]);
			$('#sixth').addClass('digit time-'+time[5]);
		}
		addClasses();
	}

	tickTock();

	setInterval(function(){
		tickTock();
	}, 1000)
	
	setTimeout(function() {
		$('body').removeClass('dn');
	}, 1000);

	setTimeout(function() {
		$('.colon').removeClass('dn');
	}, 2000);

	

})